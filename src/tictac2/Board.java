package tictac2;

class Board{
	private	char board[][] = new char[3][3];
	private int gridSpaceLeft=9;
	
	public void initBoard(){
		for(int row=0;row<board.length;row++)
			for(int col=0;col<board[0].length;col++)
				board[row][col]='-';
	}
	public boolean isGridSpaceLeft() {
		return (gridSpaceLeft>0);
	}

	public char getPos(int row, int col){
		return board[row][col];
	}
	public void setPos(int row, int col, char x){
		if(isAllowed(row, col)){
			board[row][col]=x;
			gridSpaceLeft--;
		}
	}
	
	public boolean isAllowed(int row, int col){
		return (board[row][col]=='-');
	}
	
	public boolean isGameOver(){
		return checkRows() || checkCols() || checkDiag();
	}
	
	private boolean checkDiag() {
		StringBuilder d1 = new StringBuilder();
		StringBuilder d2 = new StringBuilder();
		boolean isOver = false;
		for(int row=0,col=2;row<3 && col>=0;row++,col--){
			d1.append(board[row][row]);
			d2.append(board[row][col]);
		}
		if (d1.toString().equals("xxx") || d1.toString().equals("ooo")) isOver=true;
		if (d2.toString().equals("xxx") || d2.toString().equals("ooo")) isOver=true;
		return isOver;
	}
	
	private boolean checkCols() {
		 boolean isOver = false;
		 StringBuilder sb;
			for(int row=0;row<3;row++){
				sb = new StringBuilder();
				for(int col=0;col<3;col++){
					sb.append(board[col][row]);
				}
				if (sb.toString().equals("xxx") || sb.toString().equals("ooo")){
					isOver=true;
					break;
				}
			}
			return isOver;
	}
	
	private boolean checkRows() {
		 boolean isOver = false;
		for(int row=0;row<3;row++){
			String s = new String(board[row]);
			if (s.equals("xxx") || s.equals("ooo")) isOver = true;
		}
		return isOver;
	}
	
	public void printBoard() {
		for(int row=0;row<board.length;row++){
			for(int col=0;col<board[0].length;col++)
				System.out.print(board[row][col]);
			System.out.print("\n");
		}
	}
}